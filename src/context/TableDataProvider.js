import { createContext, useState } from "react";

export const TableContext = createContext([]);
export const TableProvider = ({children}) => {
    const [table, setTable] = useState([]);

    return (
        <TableContext.Provider value={{ table, setTable }}>
            {children}
        </TableContext.Provider>
    )
}