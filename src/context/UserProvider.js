import { createContext, useState } from "react";

export const UserContext = createContext();
export const UserProvider = ({children}) => {
    const [users, setUser] = useState([]);

    return (
        <UserContext.Provider value={{ users, setUser }}>
            {children}
        </UserContext.Provider>
    )
}