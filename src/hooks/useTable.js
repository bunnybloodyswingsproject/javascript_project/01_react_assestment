import {useContext} from "react";
import { TableContext } from "../context/TableDataProvider";

const useTable = () => {
  return useContext(TableContext);
}

export default useTable