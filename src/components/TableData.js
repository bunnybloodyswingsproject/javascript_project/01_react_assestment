import * as React from 'react';
import { styled } from '@mui/system';
import TablePaginationUnstyled, {
  tablePaginationUnstyledClasses as classes,
} from '@mui/base/TablePaginationUnstyled';
import { useRef, useState, useEffect } from 'react';
import axios from "axios";
import useTable from "../hooks/useTable";

function createData(name, calories, fat) {
  return { name, calories, fat };
}

const blue = {
  200: '#A5D8FF',
  400: '#3399FF',
};

const grey = {
  50: '#F3F6F9',
  100: '#E7EBF0',
  200: '#E0E3E7',
  300: '#CDD2D7',
  400: '#B2BAC2',
  500: '#A0AAB4',
  600: '#6F7E8C',
  700: '#3E5060',
  800: '#2D3843',
  900: '#1A2027',
};

const Root = styled('div')(
  ({ theme }) => `
  table {
    font-family: IBM Plex Sans, sans-serif;
    font-size: 0.875rem;
    border-collapse: collapse;
    width: 100%;
  }

  td,
  th {
    border: 1px solid ${theme.palette.mode === 'dark' ? grey[800] : grey[200]};
    text-align: left;
    padding: 6px;
  }

  th {
    background-color: ${theme.palette.mode === 'dark' ? grey[900] : grey[100]};
  }
  `,
);

const CustomTablePagination = styled(TablePaginationUnstyled)(
  ({ theme }) => `
  & .${classes.spacer} {
    display: none;
  }

  & .${classes.toolbar}  {
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    gap: 10px;

    @media (min-width: 768px) {
      flex-direction: row;
      align-items: center;
    }
  }

  & .${classes.selectLabel} {
    margin: 0;
  }

  & .${classes.select}{
    padding: 2px;
    border: 1px solid ${theme.palette.mode === 'dark' ? grey[800] : grey[200]};
    border-radius: 50px;
    background-color: transparent;

    &:hover {
      background-color: ${theme.palette.mode === 'dark' ? grey[800] : grey[50]};
    }

    &:focus {
      outline: 1px solid ${theme.palette.mode === 'dark' ? blue[400] : blue[200]};
    }
  }

  & .${classes.displayedRows} {
    margin: 0;

    @media (min-width: 768px) {
      margin-left: auto;
    }
  }

  & .${classes.actions} {
    padding: 2px;
    border: 1px solid ${theme.palette.mode === 'dark' ? grey[800] : grey[200]};
    border-radius: 50px;
    text-align: center;
  }

  & .${classes.actions} > button {
    margin: 0 8px;
    border: transparent;
    border-radius: 2px;
    background-color: transparent;

    &:hover {
      background-color: ${theme.palette.mode === 'dark' ? grey[800] : grey[50]};
    }

    &:focus {
      outline: 1px solid ${theme.palette.mode === 'dark' ? blue[400] : blue[200]};
    }
  }
  `,
);

export default function UnstyledTable() {
    const { table, setTable } = useTable();
    let tableRef = useRef(false);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);
    useEffect(() => {
        const fetchUsers = async () => {
            try {
                const response = await axios.get(`https://randomuser.me/api/?results=100`, {
                    headers: { "Content-Type": "application/json" }
                });
                setTable(response.data.results);
            }catch(error) {
                console.log(error)
            }
        }
        tableRef.current && fetchUsers();
        return () => tableRef.current = true
    }, []);
  const [page, setPage] = React.useState(0);

  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - table.length) : 0;

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  console.log(table)
  return (
    <div className='table_container'>
        <Root sx={{ width: 500, maxWidth: '100%' }}>
        <table aria-label="custom pagination table">
            <thead>
            <tr>
                <th>Dessert</th>
                <th>Calories</th>
                <th>Fat</th>
            </tr>
            </thead>
            <tbody>
            {(rowsPerPage > 0
                ? table.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                : table
            ).map((row) => (
                <tr key={row.login.uuid}>
                <td>{row.name.first + " " + row.name.last}</td>
                <td style={{ width: 120 }} align="right">
                    {row.email}
                </td>
                <td style={{ width: 120 }} align="right">
                    {row.phone}
                </td>
                </tr>
            ))}

            {emptyRows > 0 && (
                <tr style={{ height: 34 * emptyRows }}>
                <td colSpan={3} />
                </tr>
            )}
            </tbody>
            <tfoot>
            <tr>
                <CustomTablePagination
                rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
                colSpan={3}
                count={table.length}
                rowsPerPage={rowsPerPage}
                page={page}
                slotProps={{
                    select: {
                    'aria-label': 'rows per page',
                    },
                    actions: {
                    showFirstButton: true,
                    showLastButton: true,
                    },
                }}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
                />
            </tr>
            </tfoot>
        </table>
        </Root>
    </div>
  );
}