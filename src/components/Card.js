import React, { useEffect, useRef, useState } from 'react'
import Filter from './Filter'
import axios from "axios";
import { Link } from "react-router-dom"
import useUser from '../hooks/useUser';
const Card = () => {
    const [gender, setGender] = useState("");
    const { users, setUser } = useUser();
    const userRef = useRef(false);
    const [pagination, setPagination] = useState(10);
    const [isFilter, setIsFilter] = useState(false);

    useEffect(() => {
        const fetchUsers = async () => {
            try {
                const response = await axios.get(`api/user/list/?${gender === "" ? '' : gender === "Male" ? "&gender=male" : "&gender=female"}`, {
                    headers: { "Content-Type": "application/json" }
                });
                console.log(response.data.results)
                if(response.data.success) {
                    setUser(response.data.results);
                } 
            }catch(error) {
                console.log(error);
            }
        }
        userRef.current && fetchUsers();
        return () => userRef.current = true;
    }, [gender]);

    const onHandlerFilter = () => {
        setIsFilter(prev => !prev);
    }
    console.log(users);

    return (
    <div className="card__container">
        <div className="card__filter">
            <Filter gender={gender} setGender={setGender} setIsFilter={setIsFilter} isFilter={isFilter} />
        </div>
        <div className="card__list_container">
            <div className="card__list_filtered_link">
                <div className="card__link">
                    <small>User List</small> 
                    { 
                        gender !== "" ?
                        (
                            <div className="card_list_filtered_category">
                                <small>/</small>
                                <small>{gender}</small>
                            </div>
                        )
                        :
                        (
                            <div className="card_list_filtered_category">
                                <small>/</small>
                                <small>{gender}</small>
                            </div>
                        )
                    }
                </div>
                <div className="card_right_side">
                    <Link to="/add" className="card_risght_side_addBtn">
                        <i className="fa-solid fa-user-plus"></i>
                        <p>ADD</p>
                    </Link>
                    <div className="link_filter_icon" onClick={onHandlerFilter}>
                        <i className="fa-solid fa-filter"></i>
                        <h4>Filter</h4>
                    </div>
                </div>
            </div>
            <div className="card_list_content_list">
                {
                    users?.length === 0  ?
                    (
                        <div className="card_container_empty">
                            <div className="empty_container_content">
                                <i className="fa-regular fa-folder-open"></i>
                                <small>NO USER</small>
                            </div>
                        </div>
                    )
                    :
                    (
                        users?.map((user, i) => (
                            <div key={i} className="card_container">
                                <Link to="/edit" state={{user: user}} className="card_image_container">
                                    <img src={user.picture.thumbnail} alt="image_model" />
                                </Link>
                                <div className="card__info">
                                    <h4>{user.name.first} {user.name.last}</h4>
                                    <div className="card__small_info">
                                        <small className="info_email">{user.email}</small>
                                        <div className="card_gender">
                                            { 
                                                user.gender === "male" ?  
                                                    (<i className="fa-solid fa-mars"></i>) 
                                                    : 
                                                    (<i className="fa-solid fa-venus"></i>) }
                                            <small>{user.gender}</small>
                                        </div>
                                    </div>
                                </div>
                                <div className="deleteBtn">
                                   <i class="fa-solid fa-circle-xmark"></i>
                                </div>
                            </div>
                            ))
                    )
                }

                {
                    users?.length > 10 &&
                    (
                    <div className="page__loader">
                        <div className="button__loader" onClick={(() => setPagination(prev => prev + 10))}>
                            <p>Load More</p>
                            <i className="fa-solid fa-angles-down"></i>
                        </div>
                    </div>
                    )
                }
            </div>
        </div>
    </div>
    )
}

export default Card