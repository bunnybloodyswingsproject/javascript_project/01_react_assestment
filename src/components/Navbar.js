import React from 'react'
import { Link } from 'react-router-dom';
import useUser from '../hooks/useUser'

const Navbar = () => {
  const { users, setUser } = useUser();
  const onhandleSearch = (e) => {
    const value = e.target.value;
    const filteredUser = users.filter(user => user.name === value);
    setUser(prev => filteredUser)
  }
  return (
    <Link to="/" className="navbar">
        <div className="navbar__brand">
            <h1>RandomUser</h1>
        </div>
    </Link>
  )
}

export default Navbar