import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom';
import useUser from '../hooks/useUser';
import axios from "axios";

const Add = () => {
  const [progress, setProgress] = useState(0);
  const [progressCircle, setProgressCircle] = useState([0, 1, 2]);
  const [person, setPerson] = useState({
    gender: "male"
  });
  const { setUser } = useUser();
  const female = "https://avatars.dicebear.com/api/adventurer/gender=female/person.svg?scale=120";
  const male = "https://avatars.dicebear.com/api/adventurer/your-custom-seed.svg?scale=120";
  const [isMale, setIsMale] = useState(true);
  const navigate = useNavigate();
  const onHandleFemaleGender = () => {
    setIsMale(false);
    setPerson({...person, gender: "female"});
  }

  const onHandleMaleGender = () => {
    setIsMale(true);
    setPerson({...person, gender: "male"});
  }

  const onHandleInput = (e) => {
    const value = e.target.value;
    setPerson({...person, [e.target.name]: value});
  }

  const user_objectMapper = (
    firstname,
    lastname,
    housenumber,
    street,
    city,
    state,
    country,
    email,
    username,
    password,
    dob,
    age,
    telephone,
    cellphone,
    gender, image1, image2) => {
      let myImage = gender === "male" ? image1 : image2
      return {
        cell: cellphone,
        dob: {
          date: dob,
          age: age
        },
        email: email,
        gender: gender,
        id: {
          name: "",
          value: null
        },
        location: {
          city: city,
          coordinates: {
            latitude: "",
            longitude: ""
          },
          country: country,
          postcode: 0,
          state: state,
          street: {
            number: housenumber,
            name: street
          },
          timezone: null
        },
        login: {
          uuid: "12asda342",
          username: username,
          password: password,
          md5: "asdasd1234sdw45",
          salt: "",
          sha1: "",
          sha256: ""
        },
        nat: "NZ",
        phone: telephone,
        picture: {
          large: myImage, 
          medium: myImage,
          thumbnail: myImage
        },
        registered: {
          age: 0,
          date: ""
        },
        name: {
          first: firstname,
          last: lastname,
          title: isMale ? "Mr" : "Ms"
        }
      }
  }

  const onSubmitAdd = async (e) => {
    e.preventDefault();
    const {
      firstname,
      lastname,
      housenumber,
      street,
      city,
      state,
      country,
      email,
      username,
      password,
      dob,
      age,
      telephone,
      cellphone,
      gender
    } = person;

    if(
      (firstname === undefined || firstname === "") ||
      (lastname === undefined || lastname === "") ||
      (housenumber === undefined || housenumber === "") ||
      (street === undefined || street === "") ||
      (city === undefined || city === "") ||
      (state === undefined || state === "") ||
      (country === undefined || country === "") ||
      (email === undefined || email === "") ||
      (username === undefined || username === "") ||
      (password === undefined || password === "") ||
      (dob === undefined || dob === "") ||
      (age === undefined || age === 0) ||
      (telephone === undefined || telephone === 0) ||
      (cellphone === undefined || cellphone === 0)
    ) {
      return alert("Please complete the form.")
    }
    let myImage = gender === "male" ? male : female
    const newUser = user_objectMapper(firstname,
      lastname,
      housenumber,
      street,
      city,
      state,
      country,
      email,
      username,
      password,
      dob,
      age,
      telephone,
      cellphone, gender,male, female)
      console.log(newUser);
      
      const response = await axios.post("api/user/add",
        {
          cell: cellphone,
          dob: {
            date: dob,
            age: age
          },
          email: email,
          gender: gender,
          personalId: {
            name: "",
            value: ""
          },
          location: {
            city: city,
            coordinates: {
              latitude: "",
              longitude: ""
            },
            country: country,
            postcode: "0",
            state: state,
            street: {
              number: housenumber,
              name: street
            },
            timezone: {
              offset: "",
              description: ""
            }
          },
          login: {
            uuid: "12asda342",
            username: username,
            password: password,
            md5: "asdasd1234sdw45",
            salt: "",
            sha1: "",
            sha256: ""
          },
          nat: "NZ",
          phone: telephone,
          picture: {
            large: myImage, 
            medium: myImage,
            thumbnail: myImage
          },
          registered: {
            age: 0,
            date: ""
          },
          name: {
            first: firstname,
            last: lastname,
            title: isMale ? "Mr" : "Ms"
          }
        }
      )
    
    if(response.data.success) {
      setUser(prev => [...prev, response.data.newUser]);
    }
    navigate("/");
  }

  return (
    <div className="add_container">
        <div className="add_container_possible_header"></div>
        <div className="add_container_div">
            <div className="add_content_img_container">
              <div className="addForm_image">
                <img 
                  src={isMale ? male : female}
                  alt="add form"
                />
              </div>
              <div className="addForm_gender">
                <i className="fa-solid fa-mars-stroke" onClick={onHandleMaleGender}></i>
                <i className="fa-solid fa-venus" onClick={onHandleFemaleGender}></i>
              </div>
            </div>
            <div className="add_form">
            {
              progress === 0 && 
              (
              <>
                <div className="add_form_group_control">
                  <div className="group_control_sections">
                    <label>First Name</label>
                    <input
                      type="text"
                      name="firstname"
                      id="firstname"
                      onChange={onHandleInput}
                      placeholder={person.firstname === undefined ? "John" : person.firstname}
                    />
                  </div>
                  <div className="group_control_sections">
                    <label>Last Name</label>
                    <input
                      type="text"
                      name="lastname"
                      id="lastname"
                      onChange={onHandleInput}
                      placeholder={person.lastname === undefined ? "Smith" : person.lastname}
                    />
                  </div>
                </div>
                <div className="add_form_group_control">
                  <div className="group_control_sections">
                    <label>House No.</label>
                    <input
                      type="text"
                      name="housenumber"
                      id="housenumber"
                      onChange={onHandleInput}
                      placeholder={person.housenumber === undefined ? "149" : person.housenumber}
                    />
                  </div>
                  <div className="group_control_sections">
                    <label>Street Name</label>
                    <input
                      type="text"
                      name="street"
                      id="street"
                      onChange={onHandleInput}
                      placeholder={person.street === undefined ? "Pope Robles St." : person.street}
                    />
                  </div>
                </div>
                <div className="add_form_group_control">
                  <div className="group_control_sections">
                    <label>City</label>
                    <input
                      type="text"
                      name="city"
                      id="city"
                      placeholder={person.city === undefined ? "Colorado City" : person.city}
                      onChange={onHandleInput}
                    />
                  </div>
                  <div className="group_control_sections">
                    <label>State</label>
                    <input
                      type="text"
                      name="state"
                      id="state"
                      onChange={onHandleInput}
                      placeholder={person.state === undefined ? "Michigan" : person.state}
                    />
                  </div>
                </div>   
              </>
              )
            }
            {
              progress === 1 && 
              (
              <>
                <div className="add_form_group_control_single">
                  <div className="group_control_sections">
                    <label>Country</label>
                    <input
                      type="text"
                      name="country"
                      id="country"
                      onChange={onHandleInput}
                      placeholder={person.country === undefined ? "Washington" : person.country}
                    />
                  </div>
                </div>
                <div className="add_form_group_control">
                  <div className="group_control_sections">
                    <label>Email</label>
                    <input
                      type="text"
                      name="email"
                      id="email"
                      onChange={onHandleInput}
                      placeholder={person.email === undefined ? "johndsmith@random.com" : person.email}
                    />
                  </div>
                  <div className="group_control_sections">
                    <label>Username</label>
                    <input
                      type="text"
                      name="username"
                      id="username"
                      onChange={onHandleInput}
                      placeholder={person.username === undefined ? "jSmith" : person.username}
                    />
                  </div>
                </div>
                <div className="add_form_group_control">
                  <div className="group_control_sections">
                    <label>Password</label>
                    <input
                      type="password"
                      name="password"
                      id="password"
                      onChange={onHandleInput}
                      placeholder={person.password === undefined ? "********" : person.password}
                    />
                  </div>
                  <div className="group_control_sections">
                    <label>Date Of Birth</label>
                    <input
                      type="text"
                      name="dob"
                      id="dob"
                      onChange={onHandleInput}
                      placeholder={person.dob === undefined ? "08/24/1995" : person.dob}
                    />
                  </div>
                </div>   
              </>
              )
            }
            {
              progress === 2 && 
              (
              <>
                <div className="add_form_group_control">
                  <div className="group_control_sections">
                    <label>Age</label>
                    <input
                      type="text"
                      name="age"
                      id="age"
                      onChange={onHandleInput}
                      placeholder={person.age === undefined ? "27" : person.age}
                    />
                  </div>
                  <div className="group_control_sections">
                    <label>Telephone</label>
                    <input
                      type="text"
                      name="telephone"
                      id="telephone"
                      onChange={onHandleInput}
                      placeholder={person.telephone === undefined ? "87005671" : person.telephone}
                    />
                  </div>
                </div>  
                <div className="add_form_group_control_single">
                  <div className="group_control_sections">
                    <label>Cellphone</label>
                    <input
                      type="text"
                      name="cellphone"
                      id="cellphone"
                      onChange={onHandleInput}
                      placeholder={person.cellphone === undefined ? "09494565261" : person.cellphone}
                    />
                  </div>
                </div>
              </>
              )
            }
              <div className="addForm_Btn">
                { progress > 0 && <button className="cancelBtn" onClick={() => setProgress(prev => prev - 1)}>Previous</button> }
                {
                  progress !== 2 ?
                  (
                    <button className="nextBtn" onClick={() => setProgress(prev => prev + 1)}>Next</button>
                  ):
                  (
                    <button className="submitBtn" onClick={onSubmitAdd}>Submit</button>
                  )
                }
              </div>
              <div className="form_progress_indicator">
                {
                  progressCircle.map((progressElipsis, i) => (
                    <div key={i} className={progress >= progressElipsis ? "" : "current_progress"}></div>
                  ))
                }
              </div>
            </div>
        </div>
    </div>
  )
}

export default Add