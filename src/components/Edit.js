import React, { useState } from 'react'
import { useLocation, useNavigate } from 'react-router-dom';
import useUser from '../hooks/useUser';
import axios from "axios";

const Edit = () => {
  const [progress, setProgress] = useState(0);
  const [progressCircle, setProgressCircle] = useState([0, 1, 2]);
  const [person, setPerson] = useState({
    gender: "male"
  });
  const { setUser } = useUser();
  const [updatingUser, setUpdatingUser] = useState(useLocation()?.state?.user);
  console.log(updatingUser)
  const female = "https://avatars.dicebear.com/api/adventurer/gender=female/person.svg?scale=120";
  const male = "https://avatars.dicebear.com/api/adventurer/your-custom-seed.svg?scale=120";
  const [isMale, setIsMale] = useState(true);
  const navigate = useNavigate();
  const onHandleFemaleGender = () => {
    setIsMale(false);
    setPerson({...person, gender: "female"});
  }

  const onHandleMaleGender = () => {
    setIsMale(true);
    setPerson({...person, gender: "male"});
  }

  const onHandleInput = (e) => {
    const value = e.target.value;
    setPerson({...person, [e.target.name]: value});
  }

  const onSubmitAdd = async (e) => {
    
  }

  return (
    <div className="add_container">
        <div className="add_container_possible_header"></div>
        <div className="add_container_div">
            <div className="add_content_img_container">
              <div className="addForm_image">
                <img 
                  src={isMale ? male : female}
                  alt="add form"
                />
              </div>
              <div className="addForm_gender">
                <i className="fa-solid fa-mars-stroke" onClick={onHandleMaleGender}></i>
                <i className="fa-solid fa-venus" onClick={onHandleFemaleGender}></i>
              </div>
            </div>
            <div className="add_form">
            {
              progress === 0 && 
              (
              <>
                <div className="add_form_group_control">
                  <div className="group_control_sections">
                    <label>First Name</label>
                    <input
                      type="text"
                      name="firstname"
                      id="firstname"
                      onChange={onHandleInput}
                      placeholder={updatingUser.name.first}
                    />
                  </div>
                  <div className="group_control_sections">
                    <label>Last Name</label>
                    <input
                      type="text"
                      name="lastname"
                      id="lastname"
                      onChange={onHandleInput}
                      placeholder={updatingUser.name.first}
                    />
                  </div>
                </div>
                <div className="add_form_group_control">
                  <div className="group_control_sections">
                    <label>House No.</label>
                    <input
                      type="text"
                      name="housenumber"
                      id="housenumber"
                      onChange={onHandleInput}
                      placeholder={updatingUser.location.street.number}
                    />
                  </div>
                  <div className="group_control_sections">
                    <label>Street Name</label>
                    <input
                      type="text"
                      name="street"
                      id="street"
                      onChange={onHandleInput}
                      placeholder={updatingUser.location.street.name}
                    />
                  </div>
                </div>
                <div className="add_form_group_control">
                  <div className="group_control_sections">
                    <label>City</label>
                    <input
                      type="text"
                      name="city"
                      id="city"
                      placeholder={updatingUser.location.city}
                      onChange={onHandleInput}
                    />
                  </div>
                  <div className="group_control_sections">
                    <label>State</label>
                    <input
                      type="text"
                      name="state"
                      id="state"
                      onChange={onHandleInput}
                      placeholder={updatingUser.location.state}
                    />
                  </div>
                </div>   
              </>
              )
            }
            {
              progress === 1 && 
              (
              <>
                <div className="add_form_group_control_single">
                  <div className="group_control_sections">
                    <label>Country</label>
                    <input
                      type="text"
                      name="country"
                      id="country"
                      onChange={onHandleInput}
                      placeholder={updatingUser.location.country}
                    />
                  </div>
                </div>
                <div className="add_form_group_control">
                  <div className="group_control_sections">
                    <label>Email</label>
                    <input
                      type="text"
                      name="email"
                      id="email"
                      onChange={onHandleInput}
                      placeholder={updatingUser.email}
                    />
                  </div>
                  <div className="group_control_sections">
                    <label>Username</label>
                    <input
                      type="text"
                      name="username"
                      id="username"
                      onChange={onHandleInput}
                      placeholder={updatingUser.login.username}
                    />
                  </div>
                </div>
                <div className="add_form_group_control">
                  <div className="group_control_sections">
                    <label>Password</label>
                    <input
                      type="password"
                      name="password"
                      id="password"
                      onChange={onHandleInput}
                      placeholder={updatingUser.login.password}
                    />
                  </div>
                  <div className="group_control_sections">
                    <label>Date Of Birth</label>
                    <input
                      type="text"
                      name="dob"
                      id="dob"
                      onChange={onHandleInput}
                      placeholder={updatingUser.dob.date}
                    />
                  </div>
                </div>   
              </>
              )
            }
            {
              progress === 2 && 
              (
              <>
                <div className="add_form_group_control">
                  <div className="group_control_sections">
                    <label>Age</label>
                    <input
                      type="text"
                      name="age"
                      id="age"
                      onChange={onHandleInput}
                      placeholder={updatingUser.dob.age}
                    />
                  </div>
                  <div className="group_control_sections">
                    <label>Telephone</label>
                    <input
                      type="text"
                      name="telephone"
                      id="telephone"
                      onChange={onHandleInput}
                      placeholder={updatingUser.phone}
                    />
                  </div>
                </div>  
                <div className="add_form_group_control_single">
                  <div className="group_control_sections">
                    <label>Cellphone</label>
                    <input
                      type="text"
                      name="cellphone"
                      id="cellphone"
                      onChange={onHandleInput}
                      placeholder={updatingUser.cellphone}
                    />
                  </div>
                </div>
              </>
              )
            }
              <div className="addForm_Btn">
                { progress > 0 && <button className="cancelBtn" onClick={() => setProgress(prev => prev - 1)}>Previous</button> }
                {
                  progress !== 2 ?
                  (
                    <button className="nextBtn" onClick={() => setProgress(prev => prev + 1)}>Next</button>
                  ):
                  (
                    <button className="submitBtn" onClick={onSubmitAdd}>Submit</button>
                  )
                }
              </div>
              <div className="form_progress_indicator">
                {
                  progressCircle.map((progressElipsis, i) => (
                    <div key={i} className={progress >= progressElipsis ? "" : "current_progress"}></div>
                  ))
                }
              </div>
            </div>
        </div>
    </div>
  )
}

export default Edit