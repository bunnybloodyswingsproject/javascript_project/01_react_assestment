import React from 'react'
import useWindowDimensions from '../hooks/useWindowResizer';

const Filter = ({gender, setGender, setIsFilter, isFilter}) => {
    const onHandleGender = (e) => {
        setGender(e.target.value);
    }

    const { width } = useWindowDimensions();
    return (
    <div className={width > 767 ? "card__filter_container" : "card__filter_container card__filter_open"}
        style={{
            display: (width < 767 && isFilter) && "block",
            transform: (width < 767 && isFilter) && "translateX(200px)"
        }}
    >
        <div className="filter__header">
            <div className="filter__header_content">
                <i className="fa-solid fa-filter"></i>
                <h4>Filter</h4>
            </div>
        </div>
        <div className="filter__list">
            <div className="gender">
                <input 
                    type="checkbox"
                    name="male"
                    value="Male"
                    checked={gender === "Male"}
                    onChange={onHandleGender}
                />
                <label htmlFor='male'>Male</label>
            </div>
            <div className="gender">
                <input 
                    type="checkbox"
                    name="female"
                    value="Female"
                    checked={gender === "Female"}
                    onChange={onHandleGender}
                />
                <label htmlFor='female'>Female</label>
            </div>
            <div className="gender">
                <input 
                    type="checkbox"
                    name="clear"
                    value=""
                    checked={gender === ""}
                    onChange={onHandleGender}
                />
                <label htmlFor='clear'>clear</label>
            </div>
        </div>
    </div>
    )
}

export default Filter