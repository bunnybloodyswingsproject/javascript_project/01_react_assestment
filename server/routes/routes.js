const router = require("express").Router();
const {
    addUser,
    usersList
} = require("../controllers/user");

router.post("/add", addUser);
router.get("/list", usersList);
module.exports = router;