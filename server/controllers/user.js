const User = require("../models/User")

const addUser = async(req, res) => {
    try {
        const newUser = await User.create({...req.body});
        res.send({
            success: true,
            newUser,
            message: "Successfully added a user."
        })
    }catch(error) {
        return res.send({
            "success": false,
            "message": "Register connection timeout."
        })
    }
}

const usersList = async (req, res) => {
    try {
        let query = req.query ? req.query : "";
        const results = await User.find(query).exec();
        return res.send({
            success: true,
            message: "Successfully fetched the users.",
            results
        })
    }catch(error) {
        return res.send({
            success: false,
            message: "Users fetched failure."
        });
    }
}

module.exports = {
    addUser,
    usersList
}