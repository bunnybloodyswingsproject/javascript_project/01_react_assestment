require("dotenv").config();
const express = require("express");
const app = express();
const connectDB = require("./db/connection");
const routes = require("./routes/routes");
const bodyParser = require("body-parser");

app.use(express.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());

app.use("/api/user", routes);

app.listen(8000, () => {
    connectDB(process.env.MONGO_URI);
    console.log("Backend is running.");
})