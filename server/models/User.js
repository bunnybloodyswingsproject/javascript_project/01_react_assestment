const { string } = require("i/lib/util");
const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema({
    gender: String,
    name: {
        title: String,
        first: String,
        last: String
    },
    location: {
        street: {
            number: String,
            name: String
        },
        city: String,
        state: String,
        country: String,
        postcode: String,
        coordinates: {
            latitude: String,
            longitude: String
        },
        timezone: {
            offset: String,
            description: String
        }
    },
    email: String,
    login: {
        uuid: String,
        username: String,
        password: String,
        salt: String,
        md5: String,
        sha1: String,
        sha256: String
    },
    dob: {
        data: String,
        age: String
    },
    registered: {
        date: String,
        age: String
    },
    phone: String,
    cell: String,
    personalId: {
        name: String,
        value: String
    },
    picture: {
        large: String,
        medium: String,
        thumbnail: String
    },
    nat: String
});

module.exports = mongoose.model("User", UserSchema)